from elsy.routes.blueprint import bp
from elsy.mongo import mongo_db, Node, Edge, get_edges_by_ids
from elsy.core import Graph, dijkstra, shortest_path_to, SimpleEdge, ElsyTSPABC
from elsy.algo import ABCRoute
from flask import jsonify, request
from bson.objectid import ObjectId

@bp.route("/abc/<source>/<target>")
def abc_tsp(source, target):
    main_node_filter = {
        '$or': [
            {'jabatan': 'rt'},
            {'jabatan': 'rw'}
        ]
    }

    nodes = list(mongo_db.node.find(main_node_filter))
    inters = list(mongo_db.node.find({'jabatan': 'intersection'}))

    nodes = [str(n['_id']) for n in nodes]

    assert source in nodes

    inters = [str(n['_id']) for n in inters]

    edges = list(mongo_db.edge.find())
    edges = [SimpleEdge.fromdict(e) for e in edges]

    # return jsonify(edges)
    abc = ElsyTSPABC(nodes, inters, edges)
    abc.calc_nodes_dist()
    solution, route, variances = abc.abc(source, target, n=10, limit=10)
    edges_ids = [ e._id for e in route ]
    edges = get_edges_by_ids(edges_ids)

    return jsonify(edges)

@bp.route("/abc-route")
def abc_route():
    if 'source' not in request.args:
        raise Exception('Source not defined')
    if 'target' not in request.args:
        raise Exception('Target not defined')
    if 'length' not in request.args:
        raise Exception('Length not defined')

    nodes = list(mongo_db.node.find({
        '$or': [
            {'jabatan': 'rt'},
            {'jabatan': 'rw'}
        ]
    }))

    inters = list(mongo_db.node.find({'jabatan': 'intersection'}))
    edges = list(mongo_db.edge.find())
    edges = [SimpleEdge.fromdict(e) for e in edges]

    source = request.args['source']
    target = request.args['target']
    length = int(request.args['length'])

    nodes = [str(n['_id']) for n in nodes]

    assert source in nodes
    assert target in nodes

    inters = [str(n['_id']) for n in inters]

    abc = ABCRoute(nodes, inters, edges)
    abc.calc_nodes_dist()

    route, route_length = abc.abc_route(source, target, length)
    edges_ids = [e._id for e in route]

    edges = get_edges_by_ids(edges_ids)

    return jsonify({
        'source': source,
        'target': target,
        'length': length,
        'edges': edges,
        'route_length': route_length
    })
